$(document).ready(function () {

    function get_bic_from_iban(iban) {
        var deferred;
        deferred = new $.Deferred();

        iban = iban.replace(/ /g, "");

        $.ajax({
            url: "api/" + iban,
            method: "GET"
        }).done(function (data) {
                if (data.bic) {
                    deferred.resolve(data.bic)
                } else {
                    deferred.reject();
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                deferred.reject(jqXHR.responseJSON.message);
            })

        return deferred.promise();
    }

    var iban_field;
    var bic_field;
    var iban;
    var button;
    var message_el;

    button = $('button#submit');
    iban_field = $('#iban');
    bic_field = $('#bic');
    message_el = $('#message');

    iban_field.on('change', function () {
        iban = iban_field.val();
        if(iban === ""){
            message_el.hide();
            return;
        }

        get_bic_from_iban(iban).then(
            function (bic) { // on success
                bic_field.val(bic)
                message_el.show();
                message_el.addClass('alert-success');
                message_el.removeClass('alert-warning');
                message_el.text('Sucessfully retrieved IBAN.');
            },
            function (message) { // on fail
                bic_field.val("")
                message_el.show();
                message_el.removeClass('alert-success');
                message_el.addClass('alert-warning');
                message_el.text(message);
            },
            function () { // allways
            }
        );
    });
});
