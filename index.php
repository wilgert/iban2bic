<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>IBAN2BIC - Calculate the BIC from an IBAN</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css">

    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">IBAN2BIC</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#api">API</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div>
        <!--/.navbar-collapse -->
    </div>
</div>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Find the BIC for your IBAN</h1>

        <div class="row">
            <div class="col-lg-12">
                <label for="iban">
                    IBAN:
                    <input placeholder="NL89 INGB 0653 2368 67" style="width:16em;" class="input-lg" type="text" name="iban" id="iban">
                </label>

                <label for="bic">BIC: <input class="input-lg" name="bic" id="bic"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p><div id="message" style="display:none" class="alert alert-warning"></div></p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-8" id="api">
            <h2>Our API</h2>

            <p>You can use our api by calling http://iban2bic.nl/api/:iban</p>

            <p>The response will be:</p>
            <table class="table table-bordered">
                <tr>
                    <th>Status</th>
                    <th>HTTP Status Code</th>
                    <th>Contents</th>
                </tr>
                <tr>
                    <td>BIC found</td>
                    <td>200</td>
                    <td>{"bic": "the_found_bic"}</td>
                </tr>
                <tr>
                    <td>Error in the IBAN</td>
                    <td>400</td>
                    <td>{"message": "The corresponding message"}</td>
                </tr>
                <tr>
                    <td>BIC not found</td>
                    <td>404</td>
                    <td>{"message": "The corresponding message"}</td>
                </tr>
            </table>
        </div>
        <div class="col-lg-4">
            <h2>Supported Countries</h2>

            <p>
                Currently this api only supports banks from The Netherlands (NL) & Belgium (BE).
                More countries might be added in the future.
            </p>
            <h2 id="contact">Contact</h2>

            <p>
                Simply send an email to iban2bic@wilgert.nl
            </p>
        </div>
    </div>
</div>


<hr>

<footer class="container">
    <div class="row">
        <p>&copy; <a href="http://wilgert.nl" targen="_blank">Wilgert ICT</a> 2013</p>
    </div>
</footer>
</div> <!-- /container -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/main.js"></script>
<script src="js/vendor/spin.min.js"></script>


<script>
    var _gaq = [
        ['_setAccount', 'UA-45444120-1'],
        ['_trackPageview']
    ];
    (function (d, t) {
        var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
        g.src = '//www.google-analytics.com/ga.js';
        s.parentNode.insertBefore(g, s)
    }(document, 'script'));
</script>
</body>
</html>